import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detalle-moto',
  templateUrl: './detalle-moto.page.html',
  styleUrls: ['./detalle-moto.page.scss'],
})
export class DetalleMotoPage implements OnInit {

  data:any;

  constructor(private route: ActivatedRoute, private router: Router,public alertController: AlertController) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
        // Para ver la info desde la consola (no necesario)
        console.log(this.data);
      }
    });
  }

  ngOnInit() {
  }
  // Alert para borrar la moto y borrar moto
  async confirmarBorrarMoto() {
    const alert = await this.alertController.create({
      header: 'Seguro que quieres eliminar esta moto?',
      buttons: [
        {
          text: 'No borrar',
          role: 'cancel',
        }, {
          text: 'Borrar',
          handler: () => {
            // Botta l amoto al aceptar el alert
            fetch("http://motos.puigverd.org:80/moto/" + this.data.id, {
            // Borra la moto pero sigue en pantalla hasta que recargas la página
            "method": "DELETE",
            "mode": "cors"
            }).then(response => {this.router.navigate(["home"]);});
          }
        }
      ]
    });
    await alert.present();
  }
}
