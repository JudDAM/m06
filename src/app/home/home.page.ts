import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  motos:[];

  constructor(private router: Router, private menu: MenuController) {}
  ngOnInit() {this.getTodasMotos();}
// Todas las motos
  async getTodasMotos(){
    var respuesta = await fetch("http://motos.puigverd.org:80/motos");
    this.motos = await respuesta.json();
  }
  // Abrir el menú
  openMenu() {
    this.menu.enable(true, 'menu');
    this.menu.open('menu');
  }
  // Ir al detalle de las motos con la info de la moto seleccionada
  detalleMoto(index){
    var navigationExtras: NavigationExtras = {
      state: {parametros: this.motos[index]}
    };
    this.router.navigate(['detalle-moto'], navigationExtras);
  }
  // Botón añadir moto
  anadirMoto(){
    this.router.navigate(["add-moto"]);
  }

  // Menú filtrando motos por su marca
  async filtrarPorMarca(marca){
    if(marca=="Ducati" || marca=="Yamaha" || marca=="Honda"){
      const respuesta = await fetch("http://motos.puigverd.org/motos?marca="+marca);
      this.motos= await respuesta.json();
    }else this.getTodasMotos();
    this.menu.close('menu');
  }
}
